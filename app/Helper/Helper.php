<?php

use Illuminate\Support\Facades\Session;

if ( ! function_exists("successFlash")) {
    function successFlash(string $message) : void
    {
        Session::flash('flash_type', 'success');
        Session::flash('flash_message', $message);
    }
}

if ( ! function_exists("failFlash")) {
    function failFlash(string $message) : void
    {
        Session::flash('flash_type', 'fail');
        Session::flash('flash_message', $message);
    }
}

